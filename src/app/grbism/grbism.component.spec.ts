import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GRBISMComponent } from './grbism.component';

describe('GRBISMComponent', () => {
  let component: GRBISMComponent;
  let fixture: ComponentFixture<GRBISMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GRBISMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GRBISMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
