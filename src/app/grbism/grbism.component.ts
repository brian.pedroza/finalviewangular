import { Component, OnInit } from '@angular/core';
import { GetRecordsByMonthService } from 'src/app/services/get-records-by-month.service';

@Component({
  selector: 'app-grbism',
  templateUrl: './grbism.component.html',
  styleUrls: ['./grbism.component.scss']
})
export class GRBISMComponent implements OnInit {

  public datos: any;
  constructor(private getRecordsByMonthService : GetRecordsByMonthService) { }

  ngOnInit() {
  }

  async fnForm(data){
    console.log(data);
    this.datos = await this.getRecordsByMonthService.fnGetRecordByIsMonth(data.is,data.month);
    console.log("DATOS",this.datos);

  }

}
