import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams  } from '@angular/common/http';
import { Configuration } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class GetRecordsByDayService {

  constructor(private http: HttpClient, public config: Configuration) { 
    
  }
  readonly strUrl: string = this.config.apiUrl;
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  fnGetAllRecordsDay(){
    console.log("ENTRA AL SERVICE fnGetAllRecordsDay", this.strUrl);
    return this.http.get<any>(`${this.strUrl}/days`).toPromise();
  }
  fnPostRedordsByIsStrtDEndD(is,startDate,endDate) {
    console.log("entro al fnPostRedordsByIsStrtDEndD"+startDate+" "+endDate);
    const params = new HttpParams()
    .set('startDate', startDate)
    .set('endDate', endDate)
    .set('is', is);
    return this.http.post(`${this.strUrl}/users`,params).toPromise();
  }
  fnPostRecordsByStrtDEndD(startDate,endDate) {
    const params = new HttpParams()
    .set('startDate', startDate)
    .set('endDate', endDate);
    console.log("entro al fnPostAdminUser"+startDate+" "+endDate);
    return this.http.post(`${this.strUrl}/period`,params).toPromise();
  }
}
